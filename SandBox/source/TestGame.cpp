#include <Atlas.h>

namespace Atlas
{
	class TestGame : public Game
	{
	public:
		TestGame()
		{
			Logger::Info("Test app created");
		}

		~TestGame()
		{
			Logger::Info("Test app destroyed!");
		}

		void Init(Registry& registry)
		{
			// Seed the random number generator with the current time
			std::srand(std::time(0));
			const float SCREEN_W = 1280.0f, SCREEN_H = 800.0f;
			registry.SetResource<OrthographicCamera>(OrthographicCamera(0.0f, SCREEN_W, SCREEN_H, 0));

			std::string tilePrefixName = "E:/dev/Atlas/SandBox/resources/textures/tile-";
			std::string tileExtensionName = ".png";

			constexpr float TILE_W = 100;
			constexpr float TILE_H = 50;

			const int MAP_WIDTH = 10;
			const int MAP_HEIGHT = 10;

			constexpr float MAX_SPRITE_HEIGHT = 80;

			const float startScreenX = SCREEN_W / 2;
			const float startScreenY = 128;

			const glm::vec3 SPRITE_ROT = glm::vec3(0.0f);


			// Function to convert Cartesian coordinates to isometric coordinates
			auto TileToScreen = [&](float tileX, float tileY, float tileWidth, float tileHeight, glm::vec2 spriteDim) -> glm::vec3 {
				const float xScreen = (tileX - tileY) * (tileWidth / 2.0f);
				const float yScreen = (tileX + tileY) * (tileHeight / 2.0f) -0.5f * spriteDim.y;
				const float zScreen = 0; // (tileX + tileY); // Z component for depth sorting
				const float zOffset = MAX_SPRITE_HEIGHT - spriteDim.y;
				return glm::vec3(xScreen, yScreen + zOffset / 2, zScreen);
				};

			std::vector<std::vector<int>> grid = {
				{14, 23, 23, 23, 23, 23, 23, 23, 23, 13},
				{21, 32, 33, 33, 28, 33, 28, 33, 31, 20},
				{21, 34,  0,  0, 25, 33, 30,  1, 34, 20},
				{21, 34,  0,  0, 34,  1,  1, 10, 34, 20},
				{21, 25, 33, 33, 24, 33, 33, 33, 27, 20},
				{21, 34,  4,  7, 34, 18, 17, 10, 34, 20},
				{21, 34,  4,  7, 34, 16, 19, 10, 34, 20},
				{21, 34,  6,  8, 34, 10, 10, 10, 34, 20},
				{21, 29, 33, 33, 26, 33, 33, 33, 30, 20},
				{11, 22, 22, 22, 22, 22, 22, 22, 22, 12}
			};

			for (int y = 0; y < MAP_WIDTH; ++y)
			{
				for (int x = 0; x < MAP_HEIGHT; ++x)
				{

					std::stringstream ss;
					ss << tilePrefixName << grid[y][x] << tileExtensionName;

					// Get the file path as a string
					std::string filePath = ss.str();

					Reference<Texture>& texture = AssetManager::GetInstance().LoadTexture(filePath);
					glm::vec2 SPRITE_DIM = glm::vec2(texture->GetWidth(), texture->GetHeight());

					// Convert to isometric position
					glm::vec3 isoPos = glm::vec3(startScreenX, startScreenY, 0.0f) + TileToScreen(x, y, TILE_W, TILE_H, SPRITE_DIM);
					Entity& e = registry.CreateEntity();
					registry.AddComponent<TransformComponent>(e, isoPos, SPRITE_DIM);
					registry.AddComponent<SpriteComponent>(e, filePath);
				}
			}

			Reference<TextureAtlas> textureAtlas = LoadTextureAtlasFromJSON("E:/dev/Atlas/Sandbox/resources/json/road_tiles_atlas.json");
			Vector<Vector<std::string>> roadLayout = {
				{"straight_road_horizontal", "straight_road_horizontal", "straight_road_horizontal"},
				{"straight_road_vertical", "straight_road_vertical", "straight_road_vertical"},
				{"straight_road_vertical", "straight_road_vertical", "straight_road_vertical"}
			};

			for (int row = 0; row < roadLayout.size(); ++row)
			{
				for (int col = 0; col < roadLayout[row].size(); ++col)
				{
					std::string tileName = roadLayout[row][col];

					// Skip empty cells (no tile)
					if (tileName.empty())
						continue;

					SubtextureData subtextureData;
					try
					{
						subtextureData = textureAtlas->GetSubtexture(tileName);
					}
					catch (const std::exception& e)
					{
						Logger::Error("Tile '{0}' not found in atlas: {1}", tileName, e.what());
						continue;
					}

					// Calculate the world position of the tile
					glm::vec3 position = glm::vec3(col * subtextureData.width, row * subtextureData.height, 0.0f);
					
					Entity& e = registry.CreateEntity();
					registry.AddComponent<TransformComponent>(e, position, glm::vec2(subtextureData.width, subtextureData.height));
					registry.AddComponent<SpriteComponent>(e, textureAtlas->GetTexture().GetFilePath(), tileName);

				}
			}

			Entity& e = registry.CreateEntity();
			registry.AddComponent<TransformComponent>(e);
			registry.AddComponent<TextComponent>(e, "HELLO WORLD!", "");
		}
	};

}

Atlas::Game* Atlas::CreateGame()
{
	return new TestGame();
}
