#pragma once

// for use by Atlas applications

#include <iostream>

#include "Atlas/Game.h"
#include "Atlas/Logger.h"

// --------------------Utils---------------------
#include "Atlas/Utils/FileUtils.h"
#include "Atlas/Utils/TextureAtlasLoader.h"
// ----------------------------------------------


// --------------------Assets--------------------
#include "Assets/AssetManager.h"
// ----------------------------------------------


// --------------------Events--------------------
#include "Atlas/Event/Channel.h"
#include "Atlas/Event/Event.h"
#include "Atlas/Event/EventDispatcher.h"
#include "Atlas/Event/EventListener.h"
#include "Atlas/Event/Handler/MemberFunctionHandler.h"
// ----------------------------------------------

// -------------------Components-----------------
#include "Atlas/ECS/Components/TextComponent.h"
#include "Atlas/ECS/Components/TransformComponent.h"
#include "Atlas/ECS/Components/SpriteComponent.h"
// ----------------------------------------------

// --------------------OpenGL--------------------
#include "Atlas/OpenGL/Texture.h"
#include "Atlas/OpenGL/Renderer/Renderer.h"
// ----------------------------------------------

// --------------------ECS-----------------------
#include "Atlas/ECS/Registry.h"
// ----------------------------------------------


// --------------------EntryPoint--------------------
#include "Atlas/EntryPoint.h"
// --------------------------------------------------

// ----------------------Scenes----------------------
#include "Atlas/Scene.h"
// --------------------------------------------------