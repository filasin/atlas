#include "precomp.h"
#include "OpenGL/Window.h"

#include "glassert.h"
#include "Logger.h"

#include "Event/EventDispatcher.h"

#include "Events/EngineEvents.h"

namespace Atlas
{
	void glfw_initialisation_error(int error, const char* description)
	{
		EngineErrorEvent e;
		e.Reason = "ERROR::GLFW::ERROR:: {0}, ::DESCRIPTION:: {1}", error, description;
		// TODO: EventDispatcher::GetInstance().Dispatch(&e, &Channel::GetInstance());

		Logger::Error("ERROR::GLFW::ERROR:: {0}, ::DESCRIPTION:: {1}", error, description);
	}

	Window::Window(const WindowConfig& config) :
		m_Title(config.Title), m_Width(config.Width), m_Height(config.Height), m_BackgroundColor(config.BackgroundColor), m_IsClosed(false)
	{
		if (!Init(config))
		{
			glfwTerminate();
		}
	}

	Window::~Window()
	{
		glfwDestroyWindow(m_Window);
		glfwTerminate();
	}

	void Window::Clear() const
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glClearColor(m_BackgroundColor.x, m_BackgroundColor.y, m_BackgroundColor.z, m_BackgroundColor.w);
	}

	void Window::ProcessInput() const
	{
		glfwPollEvents();
	}

	void Window::SwapBuffers() const
	{
		glfwSwapBuffers(m_Window);
	}

	bool Window::IsClosed()
	{
		m_IsClosed = (glfwWindowShouldClose(m_Window) == 1);
		return m_IsClosed;
	}

	bool Window::Init(const WindowConfig& config)
	{
		glfwSetErrorCallback(glfw_initialisation_error);

		if (!glfwInit())
		{
			SendEngineError("ERROR::GLFW::Failed to initialise!");
			return false;
		}

		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, config.isDebugContextActive ? GLFW_TRUE : GLFW_FALSE);
		glfwWindowHint(GLFW_SAMPLES, config.Samples);

		m_Window = glfwCreateWindow(m_Width, m_Height, m_Title.c_str(), NULL, NULL);

		if (!m_Window)
		{
			SendEngineError("ERROR::GLFW::Could not create GLFW Window");
			glfwTerminate();
			return false;
		}

		glfwMakeContextCurrent(m_Window);

		glfwSetWindowUserPointer(m_Window, this);

		// Window Callback Functions

		glfwSetFramebufferSizeCallback(
			m_Window, 
			[](GLFWwindow* window, int width, int height) 
			{
				auto* win = static_cast<Window*>(glfwGetWindowUserPointer(window));
				if (win) {
					win->OnResize(width, height);
				}
			}
		);

		glfwSetKeyCallback(
			m_Window,
			[](GLFWwindow* window, int key, int scancode, int action, int mods)
			{
				auto* win = static_cast<Window*>(glfwGetWindowUserPointer(window));
				if (win) {
					win->OnKey(key, scancode, action, mods);
				}
			}
		);

		glfwSetMouseButtonCallback(
			m_Window,
			[](GLFWwindow* window, int button, int action, int mods)
			{
				auto* win = static_cast<Window*>(glfwGetWindowUserPointer(window));
				if (win) {
					win->OnMouseClick(button, action, mods);
				}
			}
		);

		glfwSetCursorPosCallback(
			m_Window,
			[](GLFWwindow* window, double xpos, double ypos)
			{
				auto* win = static_cast<Window*>(glfwGetWindowUserPointer(window));
				if (win) {
					win->OnCursorPosition(xpos, ypos);
				}
			}
		);

		// initialize GLAD using glfw
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

		// Print out some info about the graphics drivers

		Logger::Info("OpenGL version :  {0}", glGetString(GL_VERSION));
		Logger::Info("GLSL version   :  {0}", glGetString(GL_SHADING_LANGUAGE_VERSION));
		Logger::Info("Vendor         :  {0}", glGetString(GL_VENDOR));
		Logger::Info("Renderer       :  {0}", glGetString(GL_RENDERER));

		// enable OpenGL debug context if context allows for debug context
		int flags;
		glGetIntegerv(GL_CONTEXT_FLAGS, &flags);

		if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
		{
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); // makes sure errors are displayed synchronously
			glDebugMessageCallback(glDebugOutput, nullptr);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
		}

		glEnable(GL_BLEND);
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		return true;
	}

	void Window::OnResize(int width, int height)
	{
		m_Width = width;
		m_Height = height;
		glViewport(0, 0, width, height);
	}

	void Window::OnKey(int key, int scancode, int action, int mods)
	{
	}

	void Window::OnMouseClick(int button, int action, int mods)
	{
	}

	void Window::OnCursorPosition(double xpos, double ypos)
	{
	}

	void Window::SendEngineError(const std::string& reason)
	{
		EngineErrorEvent e;
		e.Reason = reason;
		// TODO: EventDispatcher::GetInstance().Dispatch(&e, &Channel::GetInstance());

		Logger::Error(reason);
	}
}
