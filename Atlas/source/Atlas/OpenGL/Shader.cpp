#include "precomp.h"
#include "Shader.h"

#include "Utils/FileUtils.h"
#include "Logger.h"

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Atlas
{
    Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath) : m_IsCompiled(false)
    {
        const std::string& vShaderCode = ReadFile(vertexPath);
        const std::string& fShaderCode = ReadFile(fragmentPath);

        const char* c_vShaderCode = vShaderCode.c_str();
        const char* c_fShaderCode = fShaderCode.c_str();
        // 2. Compile shaders
        unsigned int vertex, fragment;

        // Vertex Shader
        vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &c_vShaderCode, NULL);
        glCompileShader(vertex);
        m_IsCompiled = CheckCompileErrors(vertex, "VERTEX");

        // Fragment Shader
        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &c_fShaderCode, NULL);
        glCompileShader(fragment);
        m_IsCompiled = m_IsCompiled && CheckCompileErrors(fragment, "FRAGMENT");

        m_ShaderID = glCreateProgram();
        glAttachShader(m_ShaderID, vertex);
        glAttachShader(m_ShaderID, fragment);

        glLinkProgram(m_ShaderID);
        m_IsCompiled = m_IsCompiled && CheckCompileErrors(m_ShaderID, "PROGRAM");

        // Delete the shaders as they're linked into our program now and no longer necessary
        glDeleteShader(vertex);
        glDeleteShader(fragment);
    }
    
    void Shader::Use()
    {
        glUseProgram(m_ShaderID);
    }

    bool Shader::IsValid() const
    {
        return m_IsCompiled;
    }

    void Shader::SetUniform1f(const std::string& name, float value)
    {
        glUniform1f(GetUniformLocation(name), value);
    }
    
    void Shader::SetUniform1fv(const std::string& name, float* value, int count)
    {
        glUniform1fv(GetUniformLocation(name), count, value);
    }

    void Shader::SetUniform1i(const std::string& name, int value)
    {
        glUniform1i(GetUniformLocation(name), value);
    }

    void Shader::SetUniform1ui(const std::string& name, unsigned int value)
    {
        glUniform1ui(GetUniformLocation(name), value);
    }

    void Shader::SetUniform1iv(const std::string& name, int* value, int count)
    {
        glUniform1iv(GetUniformLocation(name), count, value);
    }

    void Shader::SetUniform2f(const std::string& name, const glm::vec2& vector)
    {
        glUniform2f(GetUniformLocation(name), vector.x, vector.y);
    }

    void Shader::SetUniform3f(const std::string& name, const glm::vec3& vector)
    {
        glUniform3f(GetUniformLocation(name), vector.x, vector.y, vector.z);
    }

    void Shader::SetUniform4f(const std::string& name, const glm::vec4& vector)
    {
        glUniform4f(GetUniformLocation(name), vector.x, vector.y, vector.z, vector.w);
    }

    void Shader::SetUniformMat4f(const std::string& name, const glm::mat4& value)
    {
        glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
    }


    int Shader::GetUniformLocation(const std::string& name)
    {
        if (m_ShaderLocationCache.find(name) != m_ShaderLocationCache.end())
            return m_ShaderLocationCache[name];

        int location = glGetUniformLocation(m_ShaderID, name.c_str());
        m_ShaderLocationCache[name] = location;
        return location;
    }

    bool Shader::CheckCompileErrors(unsigned int shader, const std::string& type)
    {
        int success;
        char infoLog[1024];
        if (type != "PROGRAM")
        {
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                glGetShaderInfoLog(shader, 1024, NULL, infoLog);
                Logger::Error("ERROR::SHADER_COMPILATION_ERROR of type: {0}\n {1}\n-- -------------------------------------------------- - --", type, infoLog );
            }
        }
        else
        {
            glGetProgramiv(shader, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(shader, 1024, NULL, infoLog);
                Logger::Error("ERROR::PROGRAM_LINKING_ERROR of type: {0}\n {1}\n-- -------------------------------------------------- - --", type, infoLog);
            }
        }

        return success;
    }
}