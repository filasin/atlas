#pragma once

#include "Core.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <unordered_map>
#include <string>

#include "OpenGL/Buffer/IndexBuffer.h"
#include "OpenGL/Buffer/VertexArray.h"
#include "OpenGL/Buffer/VertexBuffer.h"
#include "OpenGL/Buffer/VertexBufferLayout.h"
#include "OpenGL/Shader.h"
#include "OpenGL/Texture.h"

#include <ft2build.h>
#include FT_FREETYPE_H

namespace Atlas
{

	class FontRenderer {
	public:
		FontRenderer();
		~FontRenderer();

		void Init();
		bool LoadFont(const std::string& filepath, unsigned int fontSize);
		void RenderText(const std::string& text, const glm::vec2& position, float scale, const glm::vec4& color);
		void Begin(const glm::mat4& viewProjectionMatrix);
		void End();

	private:
		struct Character {
			Reference<Texture> Texture; // Glyph texture
			glm::ivec2 Size;        // Size of glyph
			glm::ivec2 Bearing;     // Offset from baseline to left/top
			unsigned int Advance;   // Offset to advance to next glyph
		};

		UnorderedMap<char, Character> m_Characters;
		glm::mat4 m_ViewProjectionMatrix;

		FT_Library m_FT_Library;

		Owner<Shader> m_Shader;
		Owner<VertexArray> m_VAO;
		Owner<VertexBuffer> m_VBO;
	};

}
