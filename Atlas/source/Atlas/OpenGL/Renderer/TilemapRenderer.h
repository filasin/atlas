#pragma once

#include "OpenGL/Renderer/Renderer.h"

namespace Atlas
{
    class TilemapRenderer
    {
    public:
        TilemapRenderer(const Texture& textureAtlas, int tileSize);
        ~TilemapRenderer();

        void Begin(const glm::mat4& viewProjectionMatrix);
        void End();
        void DrawTile(int x, int y, int tileIndex);

    private:
        Renderer m_Renderer;
        const Texture& m_TextureAtlas;
        int m_TileSize;
    };
}