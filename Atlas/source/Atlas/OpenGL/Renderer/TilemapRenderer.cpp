#include "precomp.h"
#include "TilemapRenderer.h"


namespace Atlas
{
	TilemapRenderer::TilemapRenderer(const Texture& textureAtlas, int tileSize) : m_Renderer(), m_TextureAtlas(textureAtlas), m_TileSize(tileSize)
	{
	}

	TilemapRenderer::~TilemapRenderer()
	{
	}

	void TilemapRenderer::Begin(const glm::mat4& viewProjectionMatrix)
	{
		m_Renderer.Begin(viewProjectionMatrix);
	}

	void TilemapRenderer::End()
	{
		m_Renderer.End();
	}

	// TODO : PROPER IMPL
	void TilemapRenderer::DrawTile(int x, int y, int tileIndex)
	{
	}
}

