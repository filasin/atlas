#include "precomp.h"
#include "OpenGL/Renderer/Renderer.h"

#include "OpenGL/Texture.h"

#include "Logger.h"

namespace Atlas
{
	Renderer::Renderer() :
		m_IndexCount(0),
		m_TextureSlotIndex(1)
	{
		Init();
	}

	Renderer::~Renderer()
	{
		delete[] m_VertexBufferBase;
	}

	void Renderer::Init()
	{
		m_VertexBufferBase = new Vertex[m_MaxVertices];

		unsigned int* indices = new unsigned int[m_MaxIndices];

		for (unsigned int i = 0, offset = 0; i < m_MaxIndices; i += 6, offset += 4)
		{
			indices[i + 0] = offset + 0;
			indices[i + 1] = offset + 1;
			indices[i + 2] = offset + 2;
			indices[i + 3] = offset + 2;
			indices[i + 4] = offset + 3;
			indices[i + 5] = offset + 0;
		}

		m_VAO = CreateOwner<VertexArray>();

		m_VBO = CreateOwner<VertexBuffer>(nullptr, m_MaxVertices * sizeof(Vertex));
		m_EBO = CreateOwner<IndexBuffer>(indices, m_MaxIndices);

		delete[]indices;

		VertexBufferLayout layout;
		layout.Push<float>(3); // position
		layout.Push<float>(4); // color
		layout.Push<float>(2); // texCoord
		layout.Push<float>(1); // texIndex

		m_VAO->AddBuffer(*m_VBO, layout);

		m_TextureSlots.resize(m_MaxTextureSlots, nullptr);

		m_TextureSlots[0] = CreateWhiteTexture();

		// Bind texture slots to the shader
		int samplers[m_MaxTextureSlots];
		for (int i = 0; i < m_MaxTextureSlots; i++)
		{
			samplers[i] = i;
		}

		m_Shader = CreateOwner<Shader>("E:/dev/Atlas/Atlas/Resources/DefaultShaders/sprite.vert", "E:/dev/Atlas/Atlas/Resources/DefaultShaders/sprite.frag");

		m_Shader->Use();
		m_Shader->SetUniform1iv("u_Textures", samplers, m_MaxTextureSlots);
	}

	void Renderer::Begin(const glm::mat4& viewProjectionMatrix)
	{
		m_ViewProjectionMatrix = viewProjectionMatrix;
		m_VertexBufferPtr = m_VertexBufferBase;
		m_IndexCount = 0;
		m_TextureSlotIndex = 1;
	}

	void Renderer::End()
	{
		Flush();
	}

	void Renderer::Flush()
	{
		if (m_IndexCount == 0) return;

		m_VBO->SetSubData
		(
			m_VertexBufferBase,
			0,
			(unsigned char*)m_VertexBufferPtr - (unsigned char*)m_VertexBufferBase
		);

		m_Shader->Use();

		m_Shader->SetUniformMat4f("u_ViewProjection", m_ViewProjectionMatrix);
		m_Shader->SetUniformMat4f("u_Model", glm::mat4(1.0f));

		for (unsigned int i = 0; i < m_TextureSlotIndex; i++)
		{
			m_TextureSlots[i]->Bind(i);
		}

		glDepthMask(GL_FALSE);

		m_VAO->Bind();
		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_INT, nullptr);

		glDepthMask(GL_TRUE);

		ResetBatch();
	}

	void Renderer::ResetBatch()
	{
		m_VertexBufferPtr = m_VertexBufferBase;
		m_IndexCount = 0;
		m_TextureSlotIndex = 1;
	}

	void Renderer::SetFrustumCulling(bool enabled)
	{
		m_EnableFrustumCulling = enabled;
	}


	void Renderer::AddQuadToBatch(const QuadData& quad)
	{
		
		if (m_EnableFrustumCulling && !IsQuadInFrustum(quad))
		{
			return;
		}

		// Determine the texture index
		float textureIndex = 0.0f;

		if (quad.texture)
		{
			for (unsigned int i = 1; i < m_TextureSlotIndex; i++)
			{
				if (m_TextureSlots[i] == quad.texture)
				{
					textureIndex = static_cast<float>(i);
					break;
				}
			}

			if (textureIndex == 0.0f && m_TextureSlotIndex >= m_MaxTextureSlots)
			{
				Flush();
			}

			if (textureIndex == 0.0f)
			{
				textureIndex = static_cast<float>(m_TextureSlotIndex);
				m_TextureSlots[m_TextureSlotIndex++] = quad.texture;
			}
		}

		// Build the transformation matrix
		glm::mat4 transform = glm::mat4(1.0f);
		transform = glm::translate(transform, quad.position);
		transform = glm::rotate(transform, glm::radians(quad.rotation), glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::scale(transform, glm::vec3(quad.size, 1.0f));

		constexpr glm::vec3 localPositions[4] =
		{
			{ 0.0f, 1.0f, 0.0f }, // Top-left
			{ 1.0f, 1.0f, 0.0f }, // Top-right
			{ 1.0f, 0.0f, 0.0f }, // Bottom-right
			{ 0.0f, 0.0f, 0.0f }  // Bottom-left
		};

		const glm::vec2 texCoords[4] = {
			{ quad.texCoords.x, quad.texCoords.y },
			{ quad.texCoords.z, quad.texCoords.y },
			{ quad.texCoords.z, quad.texCoords.w },
			{ quad.texCoords.x, quad.texCoords.w }
		};

		for (size_t i = 0; i < 4; i++)
		{
			glm::vec4 transformedPosition = transform * glm::vec4(localPositions[i], 1.0f);
			m_VertexBufferPtr->position = glm::vec3(transformedPosition);
			m_VertexBufferPtr->color = quad.color;
			m_VertexBufferPtr->texCoord = texCoords[i];
			m_VertexBufferPtr->texIndex = textureIndex;
			m_VertexBufferPtr++;
		}

		m_IndexCount += 6;

		if (m_IndexCount >= m_MaxIndices) {
			Flush();
		}
	}

	bool Renderer::IsQuadInFrustum(const QuadData& quad) const
	{
		return true;
	}

	void Renderer::DrawPixel(const glm::vec2& position, const glm::vec4& color)
	{
		// Call DrawQuad with fixed size (1x1) and no rotation
		DrawQuad({ position.x, position.y, 0.0f }, { 1.0f, 1.0f }, color, 0.0f);
	}

	void Renderer::DrawQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& color, float rotation)
	{
		QuadData quad = { position, size, color, nullptr, { 0.0f, 0.0f, 1.0f, 1.0f }, rotation };
		AddQuadToBatch(quad);
	}

	void Renderer::DrawSprite(const Texture& texture, const glm::vec3& position, const glm::vec2& size, float rotation)
	{
		QuadData quad = { position, size, glm::vec4(1.0f), &texture, { 0.0f, 0.0f, 1.0f, 1.0f }, rotation };
		AddQuadToBatch(quad);
	}

	void Renderer::DrawSubtexture(const Texture& texture, const glm::vec3& position, const glm::vec2& size, const glm::vec4& subtextureCoords, float rotation)
	{
		QuadData quad = { position, size, glm::vec4(1.0f), &texture, subtextureCoords, rotation };
		AddQuadToBatch(quad);
	}

	Texture* Renderer::CreateWhiteTexture()
	{
		uint32_t whitePixel = 0xFFFFFFFF; // RGBA = (255, 255, 255, 255)

		m_WhiteTexture = CreateOwner<Texture>(1, 1);
		m_WhiteTexture->SetData(&whitePixel, sizeof(uint32_t));
		return m_WhiteTexture.get();
	}

}
