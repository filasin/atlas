#pragma once

#include "Core.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "OpenGL/Buffer/IndexBuffer.h"
#include "OpenGL/Buffer/VertexArray.h"
#include "OpenGL/Buffer/VertexBuffer.h"
#include "OpenGL/Buffer/VertexBufferLayout.h"

#include "OpenGL/Shader.h"

namespace Atlas
{

	class Texture;

	class Renderer {
	public:
		Renderer();
		~Renderer();

		void Begin(const glm::mat4& viewProjectionMatrix);
		void End();

		void DrawPixel(const glm::vec2& position, const glm::vec4& color);
		void DrawQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& color, float rotation = 0.0f);
		void DrawSprite(const Texture& texture, const glm::vec3& position, const glm::vec2& size, float rotation = 0.0f);
		void DrawSubtexture(const Texture& texture, const glm::vec3& position, const glm::vec2& size, const glm::vec4& subtextureCoords, float rotation = 0.0f);


		void SetFrustumCulling(bool enabled);

	protected:
		struct Vertex {
			glm::vec3 position;
			glm::vec4 color;
			glm::vec2 texCoord;
			float texIndex;
		};

		struct QuadData {
			glm::vec3 position;
			glm::vec2 size;
			glm::vec4 color;
			const Texture* texture;
			glm::vec4 texCoords;
			float rotation;
		};

		static const unsigned int m_MaxQuads = 1000;
		static const unsigned int m_MaxVertices = m_MaxQuads * 4;
		static const unsigned int m_MaxIndices = m_MaxQuads * 6;
		static const unsigned int m_MaxTextureSlots = 16;

		bool m_EnableFrustumCulling = false;

		Owner<Shader> m_Shader;

		Owner<VertexArray> m_VAO;
		Owner<VertexBuffer> m_VBO;
		Owner<IndexBuffer> m_EBO;

		Vertex* m_VertexBufferBase;
		Vertex* m_VertexBufferPtr;

		unsigned int m_IndexCount;
		unsigned int m_TextureSlotIndex;

		Owner<Texture> m_WhiteTexture;
		Vector<const Texture*> m_TextureSlots;

		glm::mat4 m_ViewProjectionMatrix;

		void Init();
		void Flush();
		void ResetBatch();

		void AddQuadToBatch(const QuadData& quad);
		bool IsQuadInFrustum(const QuadData& quad) const;

		Texture* CreateWhiteTexture();
	};


}