#include "precomp.h"
#include "FontRenderer.h"
#include "Logger.h"

namespace Atlas
{

	FontRenderer::FontRenderer()
		: m_FT_Library(nullptr)
	{
		Init();
		if (m_FT_Library)
			LoadFont("E:/dev/Atlas/Atlas/Resources/Fonts/OpenSans-Regular.ttf", 64);
	}

	FontRenderer::~FontRenderer()
	{
		if (m_FT_Library)
		{
			FT_Done_FreeType(m_FT_Library);
		}
	}

	void FontRenderer::Init()
	{
		if (FT_Init_FreeType(&m_FT_Library))
		{
			Logger::Error("Failed to initialize FreeType library");
			return;
		}

		// Initialize OpenGL buffers and shader
		m_Shader = CreateOwner<Shader>("E:/dev/Atlas/Atlas/Resources/DefaultShaders/font.vert", "E:/dev/Atlas/Atlas/Resources/DefaultShaders/font.frag");
		m_Shader->Use();
		m_Shader->SetUniform1i("u_Texture", 0);

		m_VAO = CreateOwner<VertexArray>();
		m_VBO = CreateOwner<VertexBuffer>(nullptr, sizeof(float) * 6 * 4 * 1024); // Up to 1024 characters

		VertexBufferLayout layout;
		layout.Push<float>(2); // Position
		layout.Push<float>(2); // Texture Coordinates
		layout.Push<float>(4); // Color

		m_VAO->AddBuffer(*m_VBO, layout);
	}

	bool FontRenderer::LoadFont(const std::string& filepath, unsigned int fontSize)
	{
		FT_Face face;
		if (FT_New_Face(m_FT_Library, filepath.c_str(), 0, &face))
		{
			Logger::Error("Failed to load font: " + filepath);
			return false;
		}

		FT_Set_Pixel_Sizes(face, 0, fontSize);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		for (unsigned char c = 0; c < 128; c++)
		{
			if (FT_Load_Char(face, c, FT_LOAD_RENDER))
			{
				Logger::Error("Failed to load glyph: " + std::to_string(c));
				continue;
			}

			auto texture = CreateReference<Texture>(face->glyph->bitmap.width, face->glyph->bitmap.rows, GL_RED, GL_RED);
			texture->SetData(face->glyph->bitmap.buffer, face->glyph->bitmap.width * face->glyph->bitmap.rows);

			Character character = {
				texture,
				glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
				glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
				static_cast<unsigned int>(face->glyph->advance.x)
			};

			m_Characters[c] = character;
		}

		FT_Done_Face(face);
		return true;
	}

	void FontRenderer::RenderText(const std::string& text, const glm::vec2& position, float scale, const glm::vec4& color)
	{
		m_Shader->Use();

		m_Shader->SetUniformMat4f("u_ViewProjection", m_ViewProjectionMatrix);

		float x = position.x;
		float y = position.y;

		m_VAO->Bind();
		for (const char c : text)
		{
			if (m_Characters.find(c) == m_Characters.end())
				continue;

			Character ch = m_Characters[c];

			float xpos = x + ch.Bearing.x * scale;
			float ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

			float w = ch.Size.x * scale;
			float h = ch.Size.y * scale;

			float vertices[6][8] = {
				{ xpos,     ypos + h, 0.0f, 1.0f, color.r, color.g, color.b, color.a },
				{ xpos,     ypos,     0.0f, 0.0f, color.r, color.g, color.b, color.a },
				{ xpos + w, ypos,     1.0f, 0.0f, color.r, color.g, color.b, color.a },

				{ xpos,     ypos + h, 0.0f, 1.0f, color.r, color.g, color.b, color.a },
				{ xpos + w, ypos,     1.0f, 0.0f, color.r, color.g, color.b, color.a },
				{ xpos + w, ypos + h, 1.0f, 1.0f, color.r, color.g, color.b, color.a }
			};

			ch.Texture->Bind(0);
			m_VBO->SetSubData(vertices, 0, sizeof(vertices));

			glDrawArrays(GL_TRIANGLES, 0, 6);

			x += (ch.Advance >> 6) * scale; // Advance cursor to next glyph
		}

		m_VAO->Unbind();
	}

	void FontRenderer::Begin(const glm::mat4& viewProjectionMatrix)
	{
		m_ViewProjectionMatrix = viewProjectionMatrix;
	}

	void FontRenderer::End()
	{
		// No specific operations needed here for now
	}

}