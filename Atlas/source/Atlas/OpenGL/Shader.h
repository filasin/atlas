#pragma once

#include <string>
#include <glm/glm.hpp>

namespace Atlas
{

    class Shader
    {
    public:
        int m_ShaderID;

        Shader(const std::string& vertexPath, const std::string& fragmentPath);

        // Uses the current shader
        void Use();

        bool IsValid() const;

        void SetUniform1f(const std::string& name, float value);
        void SetUniform1fv(const std::string& name, float* value, int count);
        void SetUniform1i(const std::string& name, int value);
        void SetUniform1ui(const std::string& name, unsigned int value);
        void SetUniform1iv(const std::string& name, int* value, int count);
        void SetUniform2f(const std::string& name, const glm::vec2& vector);
        void SetUniform3f(const std::string& name, const glm::vec3& vector);
        void SetUniform4f(const std::string& name, const glm::vec4& vector);
        void SetUniformMat4f(const std::string& name, const glm::mat4& value);
    private:
        bool m_IsCompiled;
        UnorderedMap<std::string, int> m_ShaderLocationCache;
        int GetUniformLocation(const std::string& name);
        bool CheckCompileErrors(unsigned int shader, const std::string& type);

    };

}