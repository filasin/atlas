#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace Atlas {

    static void glfw_initialisation_error(int error, const char* description);

    struct WindowConfig {
        std::string Title = "Window";
        int Width = 1280;
        int Height = 800;
        glm::vec4 BackgroundColor = { 0.0f, 0.0f, 0.0f, 1.0f };
        bool isDebugContextActive = false;
        int Samples = 16;
    };

    class Window
    {
    public:

        Window(const WindowConfig& config);
        ~Window();

        void Clear() const;
        void ProcessInput() const;
        void SwapBuffers() const;
        bool IsClosed();

        inline int GetWidth() const { return m_Width; }
        inline int GetHeight() const { return m_Height; }
        inline GLFWwindow* GetGLFWwindow() const { return m_Window; }
    private:
        std::string m_Title;
        int         m_Width, m_Height;
        GLFWwindow* m_Window;
        bool        m_IsClosed;
        glm::vec4   m_BackgroundColor;
    private:
        bool Init(const WindowConfig& config);

        void OnResize(int width, int height);
        void OnKey(int key, int scancode, int action, int mods);
        void OnMouseClick(int button, int action, int mods);
        void OnCursorPosition(double xpos, double ypos);

        void SendEngineError(const std::string& reason);
    };

}

