#include "precomp.h"
#include "Texture.h"

#include "glassert.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "Logger.h"

namespace Atlas
{
	Texture::Texture(std::string path)
		: m_FilePath(path), m_IsLoaded(false), m_TID(0)
	{
		stbi_set_flip_vertically_on_load(true);
		unsigned char* image = stbi_load(m_FilePath.c_str(), &m_Width, &m_Height, &m_BPP, 0);
		if (!image)
		{
			m_IsLoaded = false;
			Logger::Error("Failed to load texture : {0}", m_FilePath);
			return;
		}
		else
		{
			Logger::Info("Image details :");
			Logger::Info("File path : {0}", m_FilePath);
			Logger::Info("Dimensions : ({0}, {1})", m_Width, m_Height);
			Logger::Info("Bits per pixel : {0}", m_BPP);
		}

		int imageFormat = GL_RGBA;
		switch (m_BPP) {
		case 1:
			imageFormat = GL_RED;
			break;
		case 3:
			imageFormat = GL_RGB;
			break;
		case 4:
			imageFormat = GL_RGBA;
			break;
		}

		m_InternalFormat = imageFormat;
		m_DataFormat = imageFormat;

		glGenTextures(1, &m_TID);
		glBindTexture(GL_TEXTURE_2D, m_TID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		GL_CALL(glTexImage2D(GL_TEXTURE_2D, 0, imageFormat, m_Width, m_Height, 0, imageFormat, GL_UNSIGNED_BYTE, image));
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);

		stbi_image_free(image);

		m_IsLoaded = true;
	}

	Texture::Texture(int width, int height, int internalFormat, int dataFormat)
		: m_Width(width), m_Height(height), m_TID(0), m_BPP(0), m_InternalFormat(internalFormat), m_DataFormat(dataFormat)
	{
		glGenTextures(1, &m_TID);
		glBindTexture(GL_TEXTURE_2D, m_TID);

		// Set texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// Allocate memory for the texture
		glTexImage2D(GL_TEXTURE_2D, 0, m_InternalFormat, m_Width, m_Height, 0, m_DataFormat, GL_UNSIGNED_BYTE, nullptr);

		m_BPP = 1; // Default to 1 byte per pixel (e.g., GL_RED)

		if (m_DataFormat == GL_RGBA) {
			m_BPP = 4; // RGBA: 4 bytes per pixel
		}
		else if (m_DataFormat == GL_RGB) {
			m_BPP = 3; // RGB: 3 bytes per pixel
		}
		else if (m_DataFormat == GL_RG) {
			m_BPP = 2; // RG: 2 bytes per pixel
		}

		m_IsLoaded = true;
	}

	Texture::~Texture()
	{
	}

	void Texture::Bind(unsigned int slotID) const
	{
		glActiveTexture(GL_TEXTURE0 + slotID);
		glBindTexture(GL_TEXTURE_2D, m_TID);
	}

	void Texture::Unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void Texture::SetData(const void* data, unsigned int size)
	{
		if (size != m_Width * m_Height * m_BPP) {
			Logger::Error("Data size does not match texture size!");
			return;
		}

		glBindTexture(GL_TEXTURE_2D, m_TID);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_Width, m_Height, m_DataFormat, GL_UNSIGNED_BYTE, data);
	}

	bool Texture::IsValid() const
	{
		return m_IsLoaded;
	}
}