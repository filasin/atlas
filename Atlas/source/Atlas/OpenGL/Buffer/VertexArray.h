#pragma once

#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

#include <glad/glad.h>

namespace Atlas
{
    class VertexBuffer;
    class VertexBufferLayout;

    class VertexArray
    {
    public:
        VertexArray();
        ~VertexArray();

        void AddBuffer(VertexBuffer& vb, const VertexBufferLayout& layout);

        void Bind() const;
        void Unbind() const;
    private:
        unsigned int m_RendererID;
    };
}