#pragma once

namespace Atlas 
{
    class VertexBuffer
    {
    public:
        VertexBuffer(const void* data, unsigned int size);
        ~VertexBuffer();

        void Bind() const;
        void Unbind() const;
        void SetSubData(const void* data, unsigned int offset, unsigned int size);
    private:
        unsigned int m_RendererID;
    };
}