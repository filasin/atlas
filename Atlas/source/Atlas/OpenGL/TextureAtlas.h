#pragma once

#include "OpenGL/Texture.h"

#include <glm/glm.hpp>

namespace Atlas
{
	struct SubtextureData
	{
		glm::vec4 uvCoords; // UV coordinates (u1, v1, u2, v2)
		int width;          // Width of the subtexture (in pixels)
		int height;         // Height of the subtexture (in pixels)
	};

	class TextureAtlas
	{
	public:
		TextureAtlas(const Texture& texture);

		void AddSubtexture(const std::string& name, int x, int y, int width, int height);
		SubtextureData  GetSubtexture(const std::string& name) const;

		const Texture& GetTexture() const { return m_Texture; }

	private:
		const Texture& m_Texture;
		int m_AtlasWidth, m_AtlasHeight;
		UnorderedMap<std::string, SubtextureData > m_Subtextures;
	};
}