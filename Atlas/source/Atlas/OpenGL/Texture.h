#pragma once

#include <glad/glad.h>

namespace Atlas
{
    class Texture
    {

    public:
        Texture(std::string path);
        Texture(int width, int height, int internalFormat = GL_RGBA8, int dataFormat = GL_RGBA);
        ~Texture();

        void Bind(unsigned int slotID) const;
        void Unbind();

        void SetData(const void* data, unsigned int size);

        bool IsValid() const;

        inline int GetWidth()               const { return m_Width; }
        inline int GetHeight()              const { return m_Height; }
        inline int GetInternalFormat()      const { return m_InternalFormat; }
        inline int GetDataFormat()          const { return m_DataFormat; }
        inline unsigned int GetID()         const { return m_TID; }
        inline std::string GetFilePath()    const { return m_FilePath; }

    private:
        unsigned int m_TID;
        int m_Width, m_Height;
        int m_InternalFormat, m_DataFormat;
        int m_BPP;
        std::string m_FilePath;
        bool m_IsLoaded;
    };
}