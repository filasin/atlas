#include "precomp.h"

#include "TextureAtlas.h"

#include "Logger.h"

namespace Atlas
{
	TextureAtlas::TextureAtlas(const Texture& texture) : m_Texture(texture)
	{
		m_AtlasWidth = texture.GetWidth();
		m_AtlasHeight = texture.GetHeight();
	}

	void TextureAtlas::AddSubtexture(const std::string& name, int x, int y, int width, int height)
	{
		
		// Check if the subtexture already exists
		if (m_Subtextures.find(name) != m_Subtextures.end())
		{
			Logger::Warn("Subtexture '{0}' already exists in atlas. Ignoring duplicate.", name);
			return;
		}

		// Convert bottom-left-based y-coordinate to top-left-based
		int adjustedY = m_AtlasHeight - (y + height);

		// Calculate UV coordinates
		float u1 = x / static_cast<float>(m_AtlasWidth);
		float v1 = adjustedY / static_cast<float>(m_AtlasHeight);
		float u2 = (x + width) / static_cast<float>(m_AtlasWidth);
		float v2 = (adjustedY + height) / static_cast<float>(m_AtlasHeight);

		m_Subtextures[name] = SubtextureData{
		   glm::vec4(u1, v1, u2, v2),
		   width,
		   height
		};
	}

	SubtextureData TextureAtlas::GetSubtexture(const std::string& name) const
	{
		auto it = m_Subtextures.find(name);
		if (it == m_Subtextures.end())
		{
			Logger::Error("Subtexture '{0}' not found in atlas.", name);
			throw std::runtime_error("Subtexture not found: " + name);
		}
		return it->second;
	}
}