#pragma once
#include <Core.h>

#include "OpenGL/Renderer/Renderer.h"

namespace Atlas
{
	class Layer {
	public:
		explicit Layer(const std::string& name = "Layer") : m_Name(name) {}
		virtual ~Layer() = default;

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate(float deltaTime) {}
		virtual void OnRender(const Renderer& renderer) {}
		virtual void OnImGuiRender() {}

		const std::string& GetName() const { return m_Name; }

	private:
		std::string m_Name;
	};

}