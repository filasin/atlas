#pragma once

#include "Camera/Camera.h"
#include "ECS/Registry.h"
#include "LayerStack.h"

namespace Atlas {

    class Scene {
    public:
        Scene(const std::string& name) : m_Name(name) {}
        virtual ~Scene() = default;

        // Lifecycle hooks
        virtual void OnAttach() {}
        virtual void OnDetach() {}
        virtual void OnUpdate(float deltaTime) = 0;
        virtual void OnRender(const Renderer& renderer) = 0;

        void PushLayer(Reference<Layer> layer);
        void PushOverlay(Reference<Layer> overlay);
        void PopLayer(Reference<Layer> layer);
        void PopOverlay(Reference<Layer> overlay);

        // Accessors
        Registry& GetRegistry() { return m_Registry; }
        Camera& GetCamera() { return *m_Camera; }
        void SetCamera(Owner<Camera> camera) { m_Camera = std::move(camera); }
        const std::string& GetName() const { return m_Name; }

    protected:
        std::string m_Name;
        Registry m_Registry;
        Owner<Camera> m_Camera;
        LayerStack m_LayerStack;
    };

}
