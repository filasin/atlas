#pragma once

#include "Core.h"

namespace Atlas
{

    class EventListener;

    class AT_ENGINE_DLL Channel
    {
    public:
        
        void AddListener(EventListener* listener)
        {
            m_Listeners.push_back(listener);
        }

        void RemoveListener(EventListener* listener)
        {
            m_Listeners.erase
            (
                std::remove(m_Listeners.begin(), m_Listeners.end(), listener), 
                m_Listeners.end()
            );
        }
        
        template<typename EventClass>
        void HandleEvent(EventClass* event);

    private:
        Channel() {}

        Channel(const Channel&) = delete;
        void operator=(const Channel&) = delete;

        Vector<EventListener*> m_Listeners;
    };

    template<typename EventClass>
    inline void Channel::HandleEvent(EventClass* event)
    {
        for (auto& listener : m_Listeners)
        {
            listener->HandleEvent<EventClass>(event);
        }
    }
}