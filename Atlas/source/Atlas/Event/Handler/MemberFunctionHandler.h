#pragma once

#include "Event/Handler/EventHandler.h"

namespace Atlas
{
    class Event;

    template<class T, class EventClass>
    class MemberFunctionHandler : public EventHandler
    {
    public:

        using MemberFunction = void (T::*)(EventClass*);

        MemberFunctionHandler(T* instance, MemberFunction memberFunction) : m_Instance{ instance }, m_MemberFunction{ memberFunction } {};

        void Call(Event* e) override {
            (m_Instance->*m_MemberFunction)(static_cast<EventClass*>(e));
        }

    private:
        T* m_Instance;
        MemberFunction m_MemberFunction;
    };
}