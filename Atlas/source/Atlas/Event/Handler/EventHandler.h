#pragma once

#include "Core.h"

namespace Atlas
{
    class Event;

    class EventHandler
    {
    public:
        void Execute(Event* e)
        {
            Call(e);
        }

        virtual void Call(Event* e) = 0;
    };
}