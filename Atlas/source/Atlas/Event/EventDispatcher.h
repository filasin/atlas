#pragma once

#include "Core.h"
#include "Event/Channel.h"
#include "Event/EventListener.h"

namespace Atlas
{
    class Channel;

    class AT_ENGINE_DLL EventDispatcher
    {
    public:
        static inline EventDispatcher& GetInstance()
        {
            static EventDispatcher dispatcher;
            return dispatcher;
        }

        template<typename EventClass>
        void Dispatch(EventClass* event, EventListener* eventListener);

        template<typename EventClass>
        void Dispatch(EventClass* event, Channel* channel);
    private:

        EventDispatcher() {}

        EventDispatcher(const EventDispatcher&) = delete;
        void operator=(const EventDispatcher&) = delete;
    };
    template<typename EventClass>
    inline void EventDispatcher::Dispatch(EventClass* event, EventListener* eventListener)
    {
        eventListener->HandleEvent<EventClass>(event);
    }
    template<typename EventClass>
    inline void EventDispatcher::Dispatch(EventClass* event, Channel* channel)
    {
        channel->HandleEvent<EventClass>(event);
    }
}