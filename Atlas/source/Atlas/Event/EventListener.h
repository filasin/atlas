#pragma once

#include "Core.h"

#include "Event/Handler/MemberFunctionHandler.h"

namespace Atlas
{
    class EventListener
    {
    public:

        using HandlerList = std::list<EventHandler*>;

        ~EventListener()
        {
            for (auto& [_, handlers] : m_Handlers)
            {
                for (auto handler : *handlers)
                {
                    delete handler;
                }
                delete handlers;
            }
        }

        template<class T, class EventClass>
        void RegisterHandler(T* instance, void (T::* memberFunction)(EventClass*));

        template<class EventClass>
        void RemoveHandlers();

        template<typename EventClass>
        void HandleEvent(EventClass* event);

    private:
        std::unordered_map<std::type_index, HandlerList*> m_Handlers;
    };

    template<class T, class EventClass>
    inline void EventListener::RegisterHandler(T* instance, void(T::* memberFunction)(EventClass*))
    {
        HandlerList* handlers = m_Handlers[typeid(EventClass)];

        if (handlers == nullptr) {
            handlers = new HandlerList();
            m_Handlers[typeid(EventClass)] = handlers;
        }

        handlers->push_back(new MemberFunctionHandler<T, EventClass>(instance, memberFunction));
    }

    template<class EventClass>
    inline void EventListener::RemoveHandlers()
    {
        auto it = m_Handlers.find(typeid(EventClass));
        if (it != m_Handlers.end())
        {
            for (auto handler : *it->second)
            {
                delete handler;
            }
            delete it->second;
            m_Handlers.erase(it);
        }
    }

    template<typename EventClass>
    inline void EventListener::HandleEvent(EventClass* event)
    {
        HandlerList* handlers = m_Handlers[typeid(EventClass)];

        if (handlers == nullptr) {
            return;
        }

        for (auto& handler : *handlers) {
            if (handler != nullptr) {
                handler->Execute(event);
            }
        }
    }

}