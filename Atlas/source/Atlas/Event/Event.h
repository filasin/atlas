#pragma once

#include "Core.h"

namespace Atlas
{
    class Event
    {
    protected:

        virtual ~Event() = default;
    };
}
