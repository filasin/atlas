#include "precomp.h"
#include "Logger.h"

#include "spdlog/sinks/stdout_color_sinks.h"

namespace Atlas
{
     Reference<spdlog::logger> Logger::m_Logger;

     Logger::Logger()
     {

     }

     Logger::~Logger()
     {

     }

     void Logger::Init()
     {
          // https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
          spdlog::set_pattern("%^[%T] %n: %v%$");

          m_Logger = spdlog::stdout_color_mt("LOGGER");
          m_Logger->set_level(spdlog::level::trace);
     }
}
