#pragma once

#ifdef AT_PLATFORM_WINDOWS

extern Atlas::Game* Atlas::CreateGame();

int main(int argc, char** argv)
{
     Atlas::Logger::Init();
     Atlas::Logger::Trace("Hello there");

     Atlas::Engine& engine = Atlas::Engine::GetInstance();
     engine.Run();
}

#endif