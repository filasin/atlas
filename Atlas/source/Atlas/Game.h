#pragma once

#include "Core.h"
#include "Engine.h"

namespace Atlas
{
    class AT_ENGINE_DLL Game
    {
    public:
        Game();
        virtual ~Game();
        virtual void Init(Registry& registry);
        virtual void SetupSystems(Engine& engine);
    };

    // To be defined in client
    Game* CreateGame();
}

