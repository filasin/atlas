#pragma once

#include "Core.h"
#include "glm/glm.hpp"

#include "OpenGL/Shader.h"
#include "OpenGL/Texture.h"
#include "OpenGL/TextureAtlas.h"

namespace Atlas
{

	class AssetManager {
	public:
		// Singleton access
		static AssetManager& GetInstance() {
			static AssetManager instance;
			return instance;
		}

		// Load and retrieve texture
		Reference<Texture> LoadTexture(const std::string& filepath);
		Reference<Texture> GetTexture(const std::string& filepath) const;

		// Load and retrieve shader
		Reference<Shader> LoadShader(const std::string& vertexPath, const std::string& fragmentPath, const std::string& key);
		Reference<Shader> GetShader(const std::string& key) const;

		Reference<TextureAtlas> LoadTextureAtlas(const std::string& filePath);
		Reference<TextureAtlas> GetTextureAtlas(const std::string& filePath);

		
		SubtextureData GetSubtexture(const std::string& atlasPath, const std::string& subtextureName);

		void Clear();

	private:

		AssetManager() = default;

		AssetManager(const AssetManager&) = delete;
		void operator=(const AssetManager&) = delete;

		// Caches for loaded assets
		UnorderedMap<std::string, Reference<Texture>> m_Textures;
		UnorderedMap<std::string, Reference<TextureAtlas>> m_Atlases;
		UnorderedMap<std::string, Reference<Shader>> m_Shaders;
	};
}