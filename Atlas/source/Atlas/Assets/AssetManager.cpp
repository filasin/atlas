#include "precomp.h"
#include "Assets/AssetManager.h"

#include "Logger.h"

namespace Atlas
{
    Reference<Texture> AssetManager::LoadTexture(const std::string& filepath)
    {
        // Check if the texture is already cached
        auto it = m_Textures.find(filepath);
        if (it != m_Textures.end())
        {
            return it->second;
        }

        // Load the texture and cache it
        auto texture = CreateReference<Texture>(filepath);
        if (!texture->IsValid())
        {
            Logger::Error("Failed to load texture: {0}", filepath);
            return nullptr;
        }

        m_Textures[filepath] = texture;
        return texture;
    }

    Reference<Texture> AssetManager::GetTexture(const std::string& filepath) const
    {
        auto it = m_Textures.find(filepath);
        if (it == m_Textures.end())
        {
            Logger::Error("Texture not found: {0}", filepath);
            return nullptr;
        }
        return it->second;
    }

    Reference<Shader> AssetManager::LoadShader(const std::string& vertexPath, const std::string& fragmentPath, const std::string& key)
    {
        // Check if the shader is already cached
        auto it = m_Shaders.find(key);
        if (it != m_Shaders.end())
        {
            return it->second;
        }

        // Load the shader and cache it
        auto shader = CreateReference<Shader>(vertexPath, fragmentPath);
        if (!shader->IsValid())
        {
            Logger::Error("Failed to load shader: {0}, {1}", vertexPath, fragmentPath);
            return nullptr;
        }

        // Create a unique key for the shader based on its paths
        if (key == "")
            m_Shaders[vertexPath + ":" + fragmentPath] = shader;
        else
            m_Shaders[key] = shader;
        return shader;
    }

    Reference<Shader> AssetManager::GetShader(const std::string& key) const
    {
        auto it = m_Shaders.find(key);
        if (it == m_Shaders.end())
        {
            Logger::Error("Shader not found: {0}", key);
            return nullptr;
        }
        return it->second;
    }

    Reference<TextureAtlas> AssetManager::LoadTextureAtlas(const std::string& filePath)
    {
        // Check if the texture atlas is already cached
        auto it = m_Atlases.find(filePath);
        if (it != m_Atlases.end())
        {
            return it->second;
        }

        // Load the texture for the atlas
        auto texture = LoadTexture(filePath);
        if (!texture)
        {
            Logger::Error("Failed to load texture for atlas: {0}", filePath);
            return nullptr;
        }

        // Create and cache the texture atlas
        auto atlas = CreateReference<TextureAtlas>(*texture);
        m_Atlases[filePath] = atlas;
        return atlas;
    }

    Reference<TextureAtlas> AssetManager::GetTextureAtlas(const std::string& filePath)
    {
        auto it = m_Atlases.find(filePath);
        if (it != m_Atlases.end())
        {
            return it->second;
        }
        return nullptr;
    }

   SubtextureData AssetManager::GetSubtexture(const std::string& atlasPath, const std::string& subtextureName)
    {
        // Check if the texture atlas is loaded
        auto it = m_Atlases.find(atlasPath);
        if (it == m_Atlases.end())
        {
            Logger::Error("Texture atlas not loaded: {0}", atlasPath);
            throw std::runtime_error("Texture atlas not loaded: " + atlasPath);
        }

        // Retrieve the subtexture coordinates
        auto atlas = it->second;
        try
        {
            return atlas->GetSubtexture(subtextureName);
        }
        catch (const std::out_of_range& e)
        {
            Logger::Error("Subtexture not found: {0} in atlas {1}", subtextureName, atlasPath);
            throw;
        }
    }

    void AssetManager::Clear()
    {
        m_Textures.clear();
        m_Shaders.clear();
        m_Atlases.clear();
    }
}

