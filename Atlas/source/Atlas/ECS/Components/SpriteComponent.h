#pragma once

#include <glm/glm.hpp>

namespace Atlas
{
	struct SpriteComponent
	{
		std::string assetID;							// ID of the texture or texture atlas
		std::string subtextureName;						// Name of the subtexture in the atlas

		glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };

		SpriteComponent() = default;

		// Constructor for a full texture (not an atlas)
		SpriteComponent(const std::string& assetID, const glm::vec4& color = { 1.0f, 1.0f, 1.0f, 1.0f })
			: assetID(assetID), color(color) {}

		// Constructor for a subtexture in an atlas
		SpriteComponent(const std::string& assetID, const std::string& subtextureName, const glm::vec4& color = { 1.0f, 1.0f, 1.0f, 1.0f })
			: assetID(assetID), color(color), subtextureName(subtextureName) {}
	};
}
