#pragma once

#include <glm/glm.hpp>

namespace Atlas
{
	struct TransformComponent
	{
        glm::vec3 position = { 0.0f, 0.0f, 0.0f }; // x, y (2D position) and z (depth for layering)
        glm::vec2 scale = { 1.0f, 1.0f };         // Width and height scale
        float rotation = 0.0f;                    // Rotation in degrees (around Z-axis for 2D)

        TransformComponent() = default;

        TransformComponent(const glm::vec3& pos, const glm::vec2& scl = { 1.0f, 1.0f }, float rot = 0.0f)
            : position(pos), scale(scl), rotation(rot) {}
	};
}