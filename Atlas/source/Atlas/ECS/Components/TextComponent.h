#pragma once

#include <string>
#include <glm/glm.hpp>

namespace Atlas
{
    struct TextComponent
    {
        std::string text;
        std::string fontAssetID;
        glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
        float scale = 1.0f;
        bool screenSpace = false;

        TextComponent() = default;

        TextComponent(const std::string& text, const std::string& fontAssetID, const glm::vec4& color = { 1.0f, 1.0f, 1.0f, 1.0f }, float scale = 1.0f, bool screenSpace = false)
            : text(text), fontAssetID(fontAssetID), color(color), scale(scale), screenSpace(screenSpace) {}
    };
}
