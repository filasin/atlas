#include "precomp.h"
#include "RenderSystem.h"

#include "Assets/AssetManager.h"

#include "ECS/Registry.h"

#include "ECS/Components/SpriteComponent.h"
#include "ECS/Components/TextComponent.h"
#include "ECS/Components/TransformComponent.h"

#include "Camera/OrthographicCamera.h"

namespace Atlas
{
	std::string RenderSystem::GetSystemName()
	{
		return std::string("RenderSystem");
	}

	void RenderSystem::Start()
	{
	}

	void RenderSystem::Run(Registry& registry, float deltaTime)
	{
		OrthographicCamera& camera = registry.GetResource<OrthographicCamera>();

		m_Renderer.Begin(camera.GetViewProjectionMatrix());
		
		View spriteView = registry.CreateView<TransformComponent, SpriteComponent>();

		spriteView.Each
		(
			[&](auto& entity)
			{
				TransformComponent& transform = registry.GetComponent<TransformComponent>(entity);
				SpriteComponent& sprite = registry.GetComponent<SpriteComponent>(entity);

				const Texture& texture = *(AssetManager::GetInstance().LoadTexture(sprite.assetID));

				if (!sprite.subtextureName.empty())
				{
					Reference<TextureAtlas>& atlas = AssetManager::GetInstance().LoadTextureAtlas(sprite.assetID);
					SubtextureData subtextureData = atlas->GetSubtexture(sprite.subtextureName);
					m_Renderer.DrawSubtexture(texture, transform.position, transform.scale, subtextureData.uvCoords, transform.rotation);
				}
				else
				{
					m_Renderer.DrawSprite(texture, transform.position, transform.scale, transform.rotation);
				}
			}
		);

		m_Renderer.End();

		View textView = registry.CreateView<TransformComponent, TextComponent>();

		m_FontRenderer.Begin(camera.GetViewProjectionMatrix());

		textView.Each
		(
			[&](auto& entity)
			{
				TransformComponent& transform = registry.GetComponent<TransformComponent>(entity);
				TextComponent& text = registry.GetComponent<TextComponent>(entity);
				m_FontRenderer.RenderText(text.text, { transform.position.x, transform.position.y }, 1.0f, {1.0f, 1.0f, 1.0f, 1.0f});

			}
		);

		m_FontRenderer.End();
	}

	void RenderSystem::Stop()
	{
	}
}
