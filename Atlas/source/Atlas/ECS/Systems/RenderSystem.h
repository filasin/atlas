#pragma once

#include "ECS/System.h"

#include "OpenGL/Renderer/Renderer.h"
#include "OpenGL/Renderer/FontRenderer.h"

namespace Atlas
{
	class RenderSystem : public System
	{
	public:
		std::string GetSystemName() override;
		void Start() override;
		void Run(Registry& registry, float deltaTime) override;
		void Stop() override;
	private:
		Renderer m_Renderer;
		FontRenderer m_FontRenderer;
	};
}