#pragma once

#include "ISparseSet.h"

namespace Atlas
{
    constexpr unsigned int MAX_UNSIGNED_INT_VALUE = 4294967295;  // Equivalent to std::numeric_limits<unsigned int>::max() for 32-bit unsigned int
    
    template<typename T>
    class SparseSet: public ISparseSet
    {
    public:

        SparseSet(unsigned int capacity = 32) : m_Capacity(capacity), m_Size(0) 
        {
            m_Sparse.resize(capacity, MAX_UNSIGNED_INT_VALUE);
            m_Dense.reserve(capacity);
            m_Data.reserve(capacity);
        }

        ~SparseSet()
        {
            m_Dense.clear();
            m_Sparse.clear();
            m_Data.clear();
        }

        void Insert(unsigned int index, const T& value)
        {
            // Only insert if the index is not already present
            if (!Contains(index))
            {
                // Ensure m_Sparse is large enough
                if (index >= m_Sparse.size())
                {
                    size_t newCapacity = std::max(static_cast<size_t>(index + 1), m_Sparse.size() * 2);
                    // Resize m_Sparse to accommodate the new index
                    m_Sparse.resize(newCapacity, MAX_UNSIGNED_INT_VALUE); // Initialize new elements with -1 or another invalid value
                    m_Capacity = newCapacity;
                }

                if (m_Size >= m_Dense.size())
                {
                    m_Dense.push_back(index);
                    m_Data.push_back(value);
                }
                else
                {
                    m_Dense[m_Size] = index;
                    m_Data[m_Size] = value;
                }
                
                m_Sparse[index] = m_Size;
                m_Size++;
            }
        }

        void Remove(unsigned int index)
        {
            assert(Contains(index));

            // Move the last element to the place of the one to be erased
            unsigned int lastIndex = m_Dense[m_Size - 1];
            m_Dense[m_Sparse[index]] = lastIndex;
            m_Data[m_Sparse[index]] = m_Data[m_Size - 1];
            m_Sparse[lastIndex] = m_Sparse[index];
            m_Size--;
        }

        bool Contains(unsigned int index) const
        {
            return index < m_Capacity && m_Sparse[index] < m_Size && m_Dense[m_Sparse[index]] == index;
        }

        T& Get(unsigned int index)
        {
            assert(Contains(index));
            return m_Data[m_Sparse[index]];
        }

        const Vector<unsigned int>& GetDenseData() const override {
            return m_Dense;
        }

        unsigned int GetSize() const { return m_Size; }

    private:
        Vector<unsigned int> m_Dense;
        Vector<unsigned int> m_Sparse;
        Vector<T> m_Data; // Vector to store additional data associated with each index
        unsigned int m_Capacity;
        unsigned int m_Size;
    };
}