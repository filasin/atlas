#pragma once

#include "Component.h"
#include "Entity.h"
#include "ISparseSet.h"
#include "Signature.h"
#include "SparseSet.h"
#include "System.h"
#include "View.h"

namespace Atlas
{

    class ISparseSet;

    class Registry
    {
    public:

        Registry();
        ~Registry();

        Entity CreateEntity();

        void DestroyEntity(const Entity& entity);

        template<typename TComponent, typename ...TArgs>
        TComponent& AddComponent(const Entity& entity, TArgs &&...args);

        template<typename TComponent>
        bool HasComponent(const Entity& entity) const;

        template<typename TComponent>
        void RemoveComponent(const Entity& entity);

        template<typename TComponent>
        TComponent& GetComponent(const Entity& entity);


        template<typename TComponent>
        SparseSet<TComponent>& GetComponents();

        template<typename TSystem, typename ...TArgs>
        TSystem& AddSystem(TArgs &&...args);

        template<typename TSystem>
        void RemoveSystem();

        template<typename TSystem>
        bool HasSystem() const;

        template<typename TSystem>
        TSystem& GetSystem() const;

        const UnorderedMap<std::type_index, Reference<System>>& GetSystems();

        template<typename... TComponents>
        View CreateView();

        template<typename T>
        void SetResource(const T& resource);

        template<typename T>
        T& GetResource();

        template<typename T>
        bool HasResource() const;

        template<typename T>
        void RemoveResource();

    private:
        unsigned int m_EntityCount = 0;

        Set<Entity> m_AddEntities;
        Set<Entity> m_DestroyEntities;

        Vector<Reference<ISparseSet>> m_ComponentPools;
        Vector<Signature> m_ECSignatures;
        UnorderedMap<std::type_index, Reference<System>> m_Systems;

        Deque<int> m_AvailableIDs;

        UnorderedMap<std::type_index, std::shared_ptr<void>> m_Resources;
    };

    template<typename TComponent, typename ...TArgs>
    inline TComponent& Registry::AddComponent(const Entity& entity, TArgs &&...args)
    {
        const auto componentID = Component<TComponent>::GetID();
        const auto entityID = entity.GetID();

        if (componentID >= m_ComponentPools.size())
        {
            m_ComponentPools.resize(componentID + 1, nullptr);
        }

        if (!m_ComponentPools[componentID])
        {
            m_ComponentPools[componentID] = CreateReference<SparseSet<TComponent>>();
        }

        auto& componentPool = *std::static_pointer_cast<SparseSet<TComponent>>(m_ComponentPools[componentID]);

        componentPool.Insert(entityID, TComponent(std::forward<TArgs>(args)...));
        m_ECSignatures[entityID].set(componentID);

        return componentPool.Get(entityID);
    }

    template<typename TComponent>
    inline bool Registry::HasComponent(const Entity& entity) const
    {
        const auto componentID = Component<TComponent>::GetID();
        const auto entityID = entity.GetID();

        // Ensure the component ID and entity ID are within valid bounds
        if (componentID >= m_ComponentPools.size() || entityID >= m_ECSignatures.size()) {
            return false;
        }

        return m_ECSignatures[entityID].test(componentID);
    }

    template<typename TComponent>
    inline void Registry::RemoveComponent(const Entity& entity)
    {
        const auto componentID = Component<TComponent>::GetID();
        const auto entityID = entity.GetID();

        Reference<SparseSet<TComponent>> componentPool = std::static_pointer_cast<SparseSet<TComponent>>(m_ComponentPools[componentID]);
        componentPool->Remove(entityID);

        m_ECSignatures[entityID].set(componentID, false);
    }

    template<typename TComponent>
    inline TComponent& Registry::GetComponent(const Entity& entity)
    {
        const ComponentID componentID = Component<TComponent>::GetID();
        const EntityID entityID = entity.GetID();
        Reference<SparseSet<TComponent>> componentPool = std::static_pointer_cast<SparseSet<TComponent>>(m_ComponentPools[componentID]);
        return componentPool->Get(entityID);
    }

    template<typename TComponent>
    inline SparseSet<TComponent>& Registry::GetComponents()
    {
        const auto componentID = Component<TComponent>::GetID();

        if (componentID >= m_ComponentPools.size() || !m_ComponentPools[componentID]) {
            static SparseSet<TComponent> emptySet; // Return a static empty set
            return emptySet;
        }

        return *std::static_pointer_cast<SparseSet<TComponent>>(m_ComponentPools[componentID]);
    }


    template<typename TSystem, typename ...TArgs>
    inline TSystem& Registry::AddSystem(TArgs &&...args)
    {
        Reference<TSystem> newSystem = CreateReference<TSystem>(std::forward<TArgs>(args)...);
        m_Systems.insert(std::make_pair(std::type_index(typeid(TSystem)), newSystem));
        return *newSystem;
    }

    template<typename TSystem>
    void Registry::RemoveSystem() {
        auto it = m_Systems.find(std::type_index(typeid(TSystem)));
        if (it != m_Systems.end()) {
            m_Systems.erase(it);
        }
    }

    template<typename TSystem>
    bool Registry::HasSystem() const {
        return m_Systems.find(std::type_index(typeid(TSystem))) != systems.end();
    }

    template<typename TSystem>
    TSystem& Registry::GetSystem() const {
        auto system = m_Systems.find(std::type_index(typeid(TSystem)));
        return *(std::static_pointer_cast<TSystem>(system->second));
    }

    template<typename ...TComponents>
    inline View Registry::CreateView()
    {
        static_assert(sizeof...(TComponents) > 0, "CreateView requires at least one component type");

        Vector<Entity> matchingEntities;

        // Generate a combined signature for the components
        Signature requiredSignature;
        ((requiredSignature.set(Component<TComponents>::GetID())), ...);

        for (EntityID entityID = 0; entityID < m_ECSignatures.size(); ++entityID)
        {
            if ((m_ECSignatures[entityID] & requiredSignature) == requiredSignature) {
                matchingEntities.push_back(Entity(entityID));
            }
        }

        return View(matchingEntities);
    }

    template<typename T>
    inline void Registry::SetResource(const T& resource) {
        m_Resources[typeid(T)] = std::make_shared<T>(resource);
    }

    template<typename T>
    inline T& Registry::GetResource() {
        auto it = m_Resources.find(typeid(T));
        if (it == m_Resources.end())
        {
            throw std::runtime_error("Resource not found!");
        }
        return *std::static_pointer_cast<T>(it->second);
    }

    template<typename T>
    inline bool Registry::HasResource() const {
        return m_Resources.find(typeid(T)) != m_Resources.end();
    }

    template<typename T>
    inline void Registry::RemoveResource() {
        m_Resources.erase(typeid(T));
    }

}