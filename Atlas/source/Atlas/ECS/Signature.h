#pragma once

#include <bitset>

namespace Atlas
{
    const unsigned int MAX_COMPONENT = 32;

    using Signature = std::bitset<MAX_COMPONENT>;
}