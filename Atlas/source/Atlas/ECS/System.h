#pragma once

#include "Component.h"
#include "Entity.h"
#include "Signature.h"

namespace Atlas
{
    class Entity;

    class System
    {
    public:
        System() = default;
        virtual ~System() = default;

        virtual std::string GetSystemName() = 0;
        virtual void Start() = 0;
        virtual void Run(Registry& registry, float deltaTime) = 0;
        virtual void Stop() = 0;

        void SetPaused(bool paused) { m_IsPaused = paused; }
        bool IsPaused() const { return m_IsPaused; }

    private:
        Signature m_ComponentSignature;
        bool m_IsPaused { false };
    };

}

