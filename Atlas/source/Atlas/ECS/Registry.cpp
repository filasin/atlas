#include "precomp.h"
#include "Registry.h"

namespace Atlas
{
	Registry::Registry()
	{
		m_AddEntities.clear();
		m_DestroyEntities.clear();

		m_ComponentPools.clear();
		m_ECSignatures.clear();
		m_Systems.clear();

		m_AvailableIDs.clear();
	}

	Registry::~Registry()
	{
		m_AddEntities.clear();
		m_DestroyEntities.clear();

		m_ComponentPools.clear();
		m_ECSignatures.clear();
		m_Systems.clear();

		m_AvailableIDs.clear();
	}

	Entity Registry::CreateEntity()
	{
		EntityID entityID = -1;

		if (m_AvailableIDs.empty())
		{
			entityID = m_EntityCount++;

			if (entityID >= m_ECSignatures.size())
			{
				m_ECSignatures.resize(entityID + 1);
			}
		}
		else
		{
			entityID = m_AvailableIDs.front();
			m_AvailableIDs.pop_front();
		}
		Entity entity(entityID);
		m_AddEntities.insert(entity);

		return entity;
	}

	void Registry::DestroyEntity(const Entity& entity)
	{
		m_ECSignatures[entity.GetID()].reset();

		for (auto pool : m_ComponentPools)
		{
			if (pool)
				pool->Remove(entity.GetID());
		}

		m_AvailableIDs.push_back(entity.GetID());
	}

	const UnorderedMap<std::type_index, Reference<System>>& Registry::GetSystems()
	{
		return m_Systems;
	}
}