#pragma once

namespace Atlas
{

    using ComponentID = unsigned int;

    class IComponent
    {
    public:
        virtual ~IComponent() = default;
    
    protected:
        static ComponentID m_NextID;
    };
}