#pragma once

namespace Atlas
{
    class ISparseSet
    {
    public:
        virtual ~ISparseSet() = default;
        virtual unsigned int GetSize() const = 0;
        virtual bool Contains(unsigned int index) const = 0;
        virtual void Remove(unsigned int index) = 0;
        virtual const Vector<unsigned int>& GetDenseData() const = 0;
    };
}