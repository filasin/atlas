#pragma once

#include "Logger.h"

namespace Atlas
{
    class Registry;
    using EntityID = unsigned int;

    class Entity
    {
    public:
        Entity(EntityID id) : m_ID(id) {};
        ~Entity() {};

        bool operator==(const Entity& other) const { return m_ID == other.GetID(); }
        bool operator!=(const Entity& other) const { return m_ID != other.GetID(); }
        bool operator> (const Entity& other) const { return m_ID > other.GetID(); }
        bool operator< (const Entity& other) const { return m_ID < other.GetID(); }
        inline EntityID GetID() const { return m_ID; }

    private:
        EntityID m_ID;
    };
}