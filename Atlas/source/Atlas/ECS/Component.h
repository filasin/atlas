#pragma once

#include "IComponent.h"

namespace Atlas
{
    template<typename TComponent>
    class Component: public IComponent
    {
    public:
        static ComponentID GetID()
        {
            static ComponentID id = m_NextID++;
            return id;
        }
    };
}