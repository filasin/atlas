#pragma once

#include "Core.h"
#include "ECS/Entity.h"

namespace Atlas
{
    class View
    {
    public:

        View(const Vector<Entity>& entities) : m_Entities(entities) {}

        template<typename Func>
        void Each(Func&& callback) const {
            for (const auto& entity : m_Entities) {
                callback(entity);
            }
        }

        const Vector<Entity>& GetEntities() const { return m_Entities; }

    private:
        Vector<Entity> m_Entities;
    };
}