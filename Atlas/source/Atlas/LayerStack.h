#pragma once

#include "Core.h"
#include "Layer.h"

namespace Atlas
{
	class Layer;

	class LayerStack {
	public:
		LayerStack() = default;
		~LayerStack();

		void PushLayer(Reference<Layer> layer);
		void PushOverlay(Reference<Layer> overlay);
		void PopLayer(Reference<Layer> layer);
		void PopOverlay(Reference<Layer> overlay);

		const Vector<Reference<Layer>>& GetLayers() const { return m_Layers; }

	private:
		Vector<Reference<Layer>> m_Layers;
		unsigned int m_LayerInsertIndex = 0; // Keeps track of where overlays begin
	};
}