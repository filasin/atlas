#pragma once

#include "Core.h"

#include "spdlog/spdlog.h"

namespace Atlas
{
    class AT_ENGINE_DLL Logger
    {
    public:
        Logger();
        ~Logger();

        static void Init();

        template<typename FormatString, typename... Args>
        inline static void Trace(const FormatString& fmt, Args&&...args);

        template<typename FormatString, typename... Args>
        inline static void Info(const FormatString& fmt, Args&&...args);

        template<typename FormatString, typename... Args>
        inline static void Warn(const FormatString& fmt, Args&&...args);

        template<typename FormatString, typename... Args>
        inline static void Error(const FormatString& fmt, Args&&...args);

    private:
        static Reference<spdlog::logger> m_Logger;
    };
}

template<typename FormatString, typename... Args>
inline void Atlas::Logger::Trace(const FormatString& fmt, Args&&...args)
{
    m_Logger->trace(fmt, std::forward<Args>(args)...);
}

template<typename FormatString, typename... Args>
inline void Atlas::Logger::Info(const FormatString& fmt, Args&&...args)
{
    m_Logger->info(fmt, std::forward<Args>(args)...);
}

template<typename FormatString, typename... Args>
inline void Atlas::Logger::Warn(const FormatString& fmt, Args&&...args)
{
    m_Logger->warn(fmt, std::forward<Args>(args)...);
}

template<typename FormatString, typename... Args>
inline void Atlas::Logger::Error(const FormatString& fmt, Args&&...args)
{
    m_Logger->error(fmt, std::forward<Args>(args)...);
}