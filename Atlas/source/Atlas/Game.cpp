#include "precomp.h"
#include "Game.h"

namespace Atlas
{
    Game::Game() {}

    Game::~Game() {}

    void Game::Init(Registry& registry) {}

    void Game::SetupSystems(Engine& engine) {}
}
