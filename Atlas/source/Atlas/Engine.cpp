#include "precomp.h"
#include "Engine.h"

#include "Game.h"

#include "Assets/AssetManager.h"

#include "Event/Channel.h"
#include "Event/EventDispatcher.h"

#include "Events/EngineEvents.h"

#include "ECS/System.h"

#include "ECS/Components/SpriteComponent.h"
#include "ECS/Components/TransformComponent.h"

#include "ECS/Systems/RenderSystem.h"

#include "Scene.h"

namespace Atlas
{
    extern Game* CreateGame();

    Engine::Engine()
    {
        WindowConfig config;
        config.Title = "Atlas Game Engine";
        config.Width = 1280;
        config.Height = 800;
        config.BackgroundColor = { 0.5f, 0.5f, 0.5f, 1.0f };
        config.isDebugContextActive = true;

        m_Window = CreateOwner<Window>(config);

        m_Game = CreateGame();
    }

    void Engine::Start()
    {
        m_Running = true;

        SetupEngineSystems();

        m_Game->SetupSystems(*this);

        for (const auto& systemItem : m_Registry.GetSystems())
        {
            const Reference<System>& system = systemItem.second;
            system->Start();
            if (m_ErrorCode != 0)
            {
                Logger::Error("Starting of the system {0} failed!", system->GetSystemName());
                break;
            }
        }
        
        m_Game->Init(m_Registry);
    }

    void Engine::Loop()
    {
        for (const auto& systemItem : m_Registry.GetSystems())
        {
            const Reference<System>& system = systemItem.second;
            if (!system->IsPaused())
            {
                system->Run(m_Registry, 0.0f);
            }
        }
    }

    void Engine::Stop()
    {
        for (const auto& systemItem : m_Registry.GetSystems())
        {
            const Reference<System>& system = systemItem.second;
            system->Stop();
            if (m_ErrorCode != 0)
            {
                Logger::Error("Stopping of the system {0} failed!", system->GetSystemName());
                break;
            }
        }
        
        delete m_Game;
    }

    void Engine::SetupEngineSystems()
    {
        m_Registry.AddSystem<RenderSystem>();
    }
    
    void Engine::HandleEngineError(EngineErrorEvent* engineErrorEvent)
    {
        Logger::Error("Engine error {0}!", engineErrorEvent->Reason);
        m_Running = false;
        m_ErrorCode = -1;
    }

    void Engine::HandleEngineShutdown(EngineShutdownEvent* engineShutdownEvent)
    {
        Logger::Info("Engine shutdown called.");
        m_Running = false;
    }

    void Engine::Run()
    {

        Start();

        while (m_Running)
        {
            m_Window->ProcessInput();

            m_Window->Clear();

            Loop();
            
            m_Window->SwapBuffers();
            m_Running = !m_Window->IsClosed();
        }

        Stop();
    }
}
