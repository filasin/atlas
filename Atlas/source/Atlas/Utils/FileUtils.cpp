#include "precomp.h"
#include "FileUtils.h"

#include "Logger.h"

std::string Atlas::ReadFile(const std::string& path)
{
    std::string content;
    std::ifstream file;

    try {
        // Ensure ifstream objects can throw exceptions:
        file.exceptions(std::ifstream::badbit);

        // Open the file
        file.open(path);

        std::stringstream fileStream;
        // Read file's buffer contents into streams
        fileStream << file.rdbuf();

        // Convert stream into string
        content = fileStream.str();

        // Close the file
        file.close();
    }
    catch (const std::ifstream::failure& e) {
        // Handle the exception and log the error
        Logger::Error("ERROR:: {0} FILE_NOT_SUCCESSFULLY_READ", path);
        // You might want to return an empty string or handle the error in your specific way
        return "";
    }

    return content;
}