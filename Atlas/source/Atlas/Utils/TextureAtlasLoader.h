#pragma once

#include "Core.h"
#include "json.hpp"

#include "Assets/AssetManager.h"
#include "FileUtils.h"
#include "Logger.h"

#include "OpenGL/TextureAtlas.h"


namespace Atlas
{
	Reference<TextureAtlas> LoadTextureAtlasFromJSON(const std::string& jsonFilePath)
	{
		try
		{
			// Read and parse the JSON file
			std::string jsonContent = ReadFile(jsonFilePath);
			nlohmann::json jsonData = nlohmann::json::parse(jsonContent);

			// Extract texture path
			std::string texturePath = jsonData["texturePath"];
			auto atlas = AssetManager::GetInstance().LoadTextureAtlas(texturePath);
			if (!atlas)
			{
				Logger::Error("Failed to load texture: {0}", texturePath);
				return nullptr;
			}

			// Extract and add subtextures
			auto subtextures = jsonData["subtextures"];
			for (auto it = subtextures.begin(); it != subtextures.end(); ++it)
			{
				std::string name = it.key();
				int x = it.value()["x"];
				int y = it.value()["y"];
				int width = it.value()["width"];
				int height = it.value()["height"];

				atlas->AddSubtexture(name, x, y, width, height);
			}

			Logger::Info("Successfully loaded TextureAtlas from {0}", jsonFilePath);
			return atlas;
		}
		catch (const std::exception& e)
		{
			Logger::Error("Failed to load TextureAtlas from {0}: {1}", jsonFilePath, e.what());
			return nullptr;
		}
	}
}

