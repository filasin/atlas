#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

namespace Atlas
{
    std::string ReadFile(const std::string& path);
}