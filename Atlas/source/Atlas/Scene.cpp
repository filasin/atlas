#include "precomp.h"
#include "Scene.h"

#include "Layer.h"

namespace Atlas
{

	void Scene::PushLayer(Reference<Layer> layer)
	{
		m_LayerStack.PushLayer(layer);
	}

	void Scene::PushOverlay(Reference<Layer> overlay)
	{
		m_LayerStack.PushOverlay(overlay);
	}

	void Scene::PopLayer(Reference<Layer> layer)
	{
		m_LayerStack.PopLayer(layer);
	}

	void Scene::PopOverlay(Reference<Layer> overlay)
	{
		m_LayerStack.PopOverlay(overlay);
	}

	void Scene::OnUpdate(float deltaTime) {
		for (auto& layer : m_LayerStack.GetLayers()) {
			layer->OnUpdate(deltaTime);
		}
	}

	void Scene::OnRender(const Renderer& renderer) {
		for (auto& layer : m_LayerStack.GetLayers()) {
			layer->OnRender(renderer);
		}
	}
}
