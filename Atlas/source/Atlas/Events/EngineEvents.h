#pragma once

#include "Event/Event.h"

namespace Atlas
{
    class EngineErrorEvent : public Event
    {
    public:
        std::string Reason;
    };

    class EngineShutdownEvent : public Event
    {
    public:
        std::string Reason;
    };

    class PreRenderEvent : public Event
    {
    };

    class RenderEvent : public Event
    {
    };

    class PostRenderEvent : public Event
    {
    };
}