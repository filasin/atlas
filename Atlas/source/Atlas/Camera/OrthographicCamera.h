#pragma once

#include "Camera/Camera.h"

namespace Atlas
{
	class OrthographicCamera : public Camera
	{
	public:
		OrthographicCamera(float left, float right, float bottom, float top);
		void SetProjection(float left, float right, float bottom, float top);
	protected:
		void RecalculateViewMatrix() override;
	};
}
