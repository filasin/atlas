#include "precomp.h"
#include "IsometricCamera.h"


namespace Atlas
{
	IsometricCamera::IsometricCamera(float width, float height)
	{
		m_ProjectionMatrix = glm::ortho(-width / 2, width / 2, -height / 2, height / 2, -1.0f, 1.0f);
		RecalculateViewMatrix();
	}

	void IsometricCamera::RecalculateViewMatrix() {
		glm::mat4 transform = glm::translate(glm::mat4(1.0f), m_Position) * 
			glm::rotate(glm::mat4(1.0f), glm::radians(m_Rotation), glm::vec3(0, 0, 1)) *
			glm::scale(glm::mat4(1.0f), glm::vec3(m_Scale, m_Scale, 1.0f));

		glm::mat4 isometricTransform = glm::rotate(glm::mat4(1.0f), glm::radians(45.0f), glm::vec3(0, 0, 1)) *
			glm::rotate(glm::mat4(1.0f), glm::radians(35.264f), glm::vec3(1, 0, 0));

		m_ViewMatrix = glm::inverse(transform * isometricTransform);
		m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}
}
