#pragma once
#include "Camera.h"

namespace Atlas
{
	class IsometricCamera : public Camera {
	public:
		IsometricCamera(float width, float height);

		void SetScale(float scale) { m_Scale = scale; RecalculateViewMatrix(); }

	protected:
		void RecalculateViewMatrix() override;

	private:
		float m_Scale = 1.0f;
	};

}

