#pragma once

#include <cassert>
#include <deque>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <typeindex>
#include <unordered_map>
#include <vector>
#include <queue>

#ifdef AT_PLATFORM_WINDOWS
    #if AT_DYNAMIC_LINK
        #ifdef AT_DLL_EXPORT
            #define AT_ENGINE_DLL _declspec(dllexport)
        #elif AT_DLL_IMPORT
            #define AT_ENGINE_DLL _declspec(dllimport)
        #endif
    #else
        #define AT_ENGINE_DLL
    #endif
#else
    #error Atlas only supports Windows!
#endif


namespace Atlas
{
    template<typename T>
    using Reference = std::shared_ptr<T>;

    template<typename T>
    using Owner = std::unique_ptr<T>;

    template<typename T, typename... Args>
    Reference<T> CreateReference(Args&&... args) {
        return Reference<T>(new T(std::forward<Args>(args)...));
    }

    template<typename T, typename... Args>
    Owner<T> CreateOwner(Args&&... args) {
        return Owner<T>(new T(std::forward<Args>(args)...));
    }

    template<typename T>
    constexpr typename std::remove_reference<T>::type&& TakeOwner(T&& t) noexcept {
        return std::move(t);
    }

    template<typename T>
    using Set = std::set<T>;

    template<typename T>
    using Vector = std::vector<T>;

    template<typename T, typename U>
    using Map = std::map<T, U>;

    template<typename T, typename U>
    using UnorderedMap = std::unordered_map<T, U>;

    template<typename T>
    using Deque = std::deque<T>;

    template<typename T>
    using Queue = std::queue<T>;
}