#pragma once

#include "ECS/Registry.h"

#include "Camera/OrthographicCamera.h"

#include "OpenGL/Window.h"
#include "OpenGL/Renderer/Renderer.h"

namespace Atlas
{

    class EngineErrorEvent;
    class EngineShutdownEvent;

    class Game;

    class Scene;

    class Engine
    {
    public:

        static Engine& GetInstance()
        {
            static Engine engine;
            return engine;
        }

        void Run();

    private:

        Engine();

        Engine(const Engine&) = delete;
        void operator=(const Engine&) = delete;

        void Start();
        void Loop();
        void Stop();

        void SetupEngineSystems();

        void HandleEngineError(EngineErrorEvent* engineErrorEvent);
        void HandleEngineShutdown(EngineShutdownEvent* engineShutdownEvent);

    private:
        Registry m_Registry;
        Game* m_Game;
        Owner<Window> m_Window;
        bool m_Running = false;
        char m_ErrorCode = 0;
    };
}

