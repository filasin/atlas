#version 330 core

// Input attributes from the VBO
layout(location = 0) in vec2 a_Position;  // Position of the vertex
layout(location = 1) in vec2 a_TexCoord; // Texture coordinate
layout(location = 2) in vec4 a_Color;    // Vertex color

// Uniforms
uniform mat4 u_ViewProjection; // View-projection matrix

// Outputs to the fragment shader
out vec2 v_TexCoord; // Interpolated texture coordinate
out vec4 v_Color;    // Interpolated color

void main()
{
    // Transform the vertex position
    gl_Position = u_ViewProjection * vec4(a_Position, 0.0, 1.0);

    // Pass texture coordinates and color to the fragment shader
    v_TexCoord = a_TexCoord;
    v_Color = a_Color;
}
