#version 330 core

layout(location = 0) in vec3 a_Position;  // Vertex position
layout(location = 1) in vec4 a_Color;     // Vertex color
layout(location = 2) in vec2 a_TexCoord;  // Texture coordinates
layout(location = 3) in float a_TexIndex; // Texture index

out vec4 v_Color;
out vec2 v_TexCoord;
out float v_TexIndex;

uniform mat4 u_ViewProjection; // View-Projection matrix
uniform mat4 u_Model;          // Model matrix

void main()
{
    gl_Position = u_ViewProjection * u_Model * vec4(a_Position, 1.0);
    v_Color = a_Color;
    v_TexCoord = a_TexCoord;
    v_TexIndex = a_TexIndex;
}