#version 330 core

// Inputs from the vertex shader
in vec2 v_TexCoord; // Interpolated texture coordinate
in vec4 v_Color;    // Interpolated color

// Texture sampler
uniform sampler2D u_Texture; // Glyph texture atlas

// Output color
out vec4 FragColor;

void main()
{
    // Sample the texture to get the alpha value
    float alpha = texture(u_Texture, v_TexCoord).r;

    // Final color is the vertex color modulated by the alpha from the texture
    FragColor = vec4(v_Color.rgb, v_Color.a * alpha);
}
