workspace "Atlas"
    architecture "x64"

    startproject "Sandbox"

    configurations
    {
        "Debug",
        "Release",
        "Final"
    }

    outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
    IncludeDir = {}
    IncludeDir["glfw"] = "Atlas/vendor/glfw/include"
    IncludeDir["glad"] = "Atlas/vendor/glad/include"
    IncludeDir["glm"] = "Atlas/vendor/glm"
    IncludeDir["stb_image"] = "Atlas/vendor/stb_image"
    IncludeDir["json"] = "Atlas/vendor/json"
    IncludeDir["freetype"] = "Atlas/vendor/freetype/include"

    group "Dependencies"
        include "Atlas/vendor/glfw"
        include "Atlas/vendor/glad"
    group ""

project "Atlas"
    location "Atlas"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    pchheader "precomp.h"
    pchsource "Atlas/source/precomp.cpp"

    files
    {
        "%{prj.name}/source/**.h",
        "%{prj.name}/source/**.cpp",
		"%{prj.name}/vendor/glm/glm/**.hpp",
		"%{prj.name}/vendor/glm/glm/**.inl",
        "%{prj.name}/vendor/stb_image/**.h",
		"%{prj.name}/vendor/stb_image/**.cpp",
		"%{prj.name}/vendor/json/**.hpp",
        "%{prj.name}/vendor/freetype/src/autofit/autofit.c",
        "%{prj.name}/vendor/freetype/src/bdf/bdf.c",
        "%{prj.name}/vendor/freetype/src/cff/cff.c",
        "%{prj.name}/vendor/freetype/src/base/ftbase.c",
        "%{prj.name}/vendor/freetype/src/base/ftbitmap.c",
        "%{prj.name}/vendor/freetype/src/cache/ftcache.c",
        "%{prj.name}/vendor/freetype/src/base/ftfstype.c",
        "%{prj.name}/vendor/freetype/src/base/ftgasp.c",
        "%{prj.name}/vendor/freetype/src/base/ftglyph.c",
        "%{prj.name}/vendor/freetype/src/gzip/ftgzip.c",
        "%{prj.name}/vendor/freetype/src/base/ftinit.c",
        "%{prj.name}/vendor/freetype/src/lzw/ftlzw.c",
        "%{prj.name}/vendor/freetype/src/base/ftstroke.c",
        "%{prj.name}/vendor/freetype/src/base/ftsystem.c",
        "%{prj.name}/vendor/freetype/src/smooth/smooth.c",
        "%{prj.name}/vendor/freetype/src/base/ftbbox.c",
        "%{prj.name}/vendor/freetype/src/base/ftmm.c",
        "%{prj.name}/vendor/freetype/src/base/ftpfr.c",
        "%{prj.name}/vendor/freetype/src/base/ftsynth.c",
        "%{prj.name}/vendor/freetype/src/base/fttype1.c",
        "%{prj.name}/vendor/freetype/src/base/ftwinfnt.c",
        "%{prj.name}/vendor/freetype/src/base/ftlcdfil.c",
        "%{prj.name}/vendor/freetype/src/base/ftgxval.c",
        "%{prj.name}/vendor/freetype/src/base/ftotval.c",
        "%{prj.name}/vendor/freetype/src/base/ftpatent.c",
        "%{prj.name}/vendor/freetype/src/pcf/pcf.c",
        "%{prj.name}/vendor/freetype/src/pfr/pfr.c",
        "%{prj.name}/vendor/freetype/src/psaux/psaux.c",
        "%{prj.name}/vendor/freetype/src/pshinter/pshinter.c",
        "%{prj.name}/vendor/freetype/src/psnames/psmodule.c",
        "%{prj.name}/vendor/freetype/src/raster/raster.c",
        "%{prj.name}/vendor/freetype/src/sfnt/sfnt.c",
        "%{prj.name}/vendor/freetype/src/truetype/truetype.c",
        "%{prj.name}/vendor/freetype/src/type1/type1.c",
        "%{prj.name}/vendor/freetype/src/cid/type1cid.c",
        "%{prj.name}/vendor/freetype/src/type42/type42.c",
        "%{prj.name}/vendor/freetype/src/winfonts/winfnt.c",
        "%{prj.name}/vendor/freetype/include/ft2build.h",
        "%{prj.name}/vendor/freetype/include/freetype/config/ftconfig.h",
        "%{prj.name}/vendor/freetype/include/freetype/config/ftheader.h",
        "%{prj.name}/vendor/freetype/include/freetype/config/ftmodule.h",
        "%{prj.name}/vendor/freetype/include/freetype/config/ftoption.h",
        "%{prj.name}/vendor/freetype/include/freetype/config/ftstdlib.h",
    }
    
    defines
    {
        "_SILENCE_STDEXT_ARR_ITERS_DEPRECATION_WARNING",
        "_SILENCE_ALL_MS_EXT_DEPRECATION_WARNINGS",
		"_CRT_SECURE_NO_WARNINGS",
        "FT2_BUILD_LIBRARY"
    }

    includedirs
    {
        "%{prj.name}/vendor/spdlog/include",
        "%{prj.name}/source/Atlas",
        "%{prj.name}/source/",
        "%{IncludeDir.glfw}",
        "%{IncludeDir.glad}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.stb_image}",
        "%{IncludeDir.json}",
        "%{IncludeDir.freetype}"
    }

    links
    {
        "glfw",
        "glad",
        "opengl32.lib"
    }

    filter "files:Atlas/vendor/freetype/**.c" -- CHANGE START: Fix precompiled header issues for FreeType
        flags { "NoPCH" } -- Disable precompiled headers for FreeType files


    filter "system:windows"
        staticruntime "on"
        systemversion "latest"

        defines
        {
            "AT_PLATFORM_WINDOWS",
            "AT_DLL_EXPORT",
            "GLFW_INCLUDE_NONE",
        }
    
    filter "action:vs*"
        toolset "v143"

    filter "configurations:Debug"
        defines "AT_DEBUG"
        runtime "Debug"
        symbols "on"
        links { "Atlas/vendor/freetype/objs/x64/Debug Static/freetype.lib" }

    filter "configurations:Release"
        defines "AT_RELEASE"
        runtime "Release"
        optimize "on"
        links { "Atlas/vendor/freetype/objs/x64/Release Static/freetype.lib" }

    filter "configurations:Final"
        defines "AT_FINAL"
        runtime "Release"
        optimize "on"
        links { "Atlas/vendor/freetype/objs/x64/Release Static/freetype.lib" }

project "Sandbox"
    location "Sandbox"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/source/**.h",
        "%{prj.name}/source/**.cpp"
    }

    defines
    {
        "_SILENCE_STDEXT_ARR_ITERS_DEPRECATION_WARNING",
        "_SILENCE_ALL_MS_EXT_DEPRECATION_WARNINGS",
        "_CRT_SECURE_NO_WARNINGS"
    }

    includedirs
    {
        "Atlas/vendor/spdlog/include",
        "Atlas/source",
        "Atlas/source/Atlas",
        "Atlas/vendor",
        "%{IncludeDir.glfw}",
        "%{IncludeDir.glad}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.freetype}",
        "%{IncludeDir.json}"
    }

    links
    {
        "Atlas"
    }

    filter "system:windows"
        systemversion "latest"

        defines
        {
            "AT_PLATFORM_WINDOWS",
            "AT_DLL_IMPORT",
        }
    
    filter "action:vs*"
        toolset "v143"

    filter "configurations:Debug"
        defines "AT_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "AT_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Final"
        defines "AT_FINAL"
        runtime "Release"
        optimize "on"